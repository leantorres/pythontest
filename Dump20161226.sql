-- MySQL dump 10.13  Distrib 5.7.15, for Linux (x86_64)
--
-- Host: localhost    Database: turnoslicencia
-- ------------------------------------------------------
-- Server version	5.7.15-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agenda`
--

DROP TABLE IF EXISTS `agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date DEFAULT NULL,
  `tipo_tramite_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_agenda_tipo_tramite_id` (`tipo_tramite_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agenda`
--

LOCK TABLES `agenda` WRITE;
/*!40000 ALTER TABLE `agenda` DISABLE KEYS */;
INSERT INTO `agenda` VALUES (1,'2016-11-14',NULL,1),(2,'2016-11-14',NULL,1);
/*!40000 ALTER TABLE `agenda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dia_no_laborable`
--

DROP TABLE IF EXISTS `dia_no_laborable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dia_no_laborable` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha_dia` date NOT NULL,
  `agenda_id` bigint(20) DEFAULT NULL,
  `hora_fin` time DEFAULT NULL,
  `hora_inicio` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dia_no_laborable_agenda_id` (`agenda_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dia_no_laborable`
--

LOCK TABLES `dia_no_laborable` WRITE;
/*!40000 ALTER TABLE `dia_no_laborable` DISABLE KEYS */;
/*!40000 ALTER TABLE `dia_no_laborable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `duracion_turno`
--

DROP TABLE IF EXISTS `duracion_turno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duracion_turno` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantidad_minutos` int(11) NOT NULL,
  `dia_semana` varchar(255) DEFAULT NULL,
  `agenda_id` bigint(20) DEFAULT NULL,
  `franja_horaria_id` bigint(20) DEFAULT NULL,
  `cantidad_cupo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_duracion_turno_agenda_id` (`agenda_id`),
  KEY `fk_duracion_turno_franja_horaria_id` (`franja_horaria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `duracion_turno`
--

LOCK TABLES `duracion_turno` WRITE;
/*!40000 ALTER TABLE `duracion_turno` DISABLE KEYS */;
INSERT INTO `duracion_turno` VALUES (1,60,'LUNES',1,1,1),(2,60,'JUEVES',1,2,2);
/*!40000 ALTER TABLE `duracion_turno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_turno`
--

DROP TABLE IF EXISTS `estado_turno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_turno` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_estado_turno` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_turno`
--

LOCK TABLES `estado_turno` WRITE;
/*!40000 ALTER TABLE `estado_turno` DISABLE KEYS */;
INSERT INTO `estado_turno` VALUES (1,'BLOQUEADO'),(2,'RESERVADO'),(3,'CONFIRMADO'),(4,'FINALIZADO');
/*!40000 ALTER TABLE `estado_turno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `franja_horaria`
--

DROP TABLE IF EXISTS `franja_horaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `franja_horaria` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hora_inicio` time NOT NULL,
  `hora_fin` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `franja_horaria`
--

LOCK TABLES `franja_horaria` WRITE;
/*!40000 ALTER TABLE `franja_horaria` DISABLE KEYS */;
INSERT INTO `franja_horaria` VALUES (1,'08:00:00','10:00:00'),(2,'12:00:00','22:00:00');
/*!40000 ALTER TABLE `franja_horaria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informacion`
--

DROP TABLE IF EXISTS `informacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informacion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  `mas_informacion` varchar(255) DEFAULT NULL,
  `tipo_tramite_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_informacion_tipo_tramite_id` (`tipo_tramite_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informacion`
--

LOCK TABLES `informacion` WRITE;
/*!40000 ALTER TABLE `informacion` DISABLE KEYS */;
INSERT INTO `informacion` VALUES (1,'Desc 1','<div style=\"color:blue\">Nico</div>',1),(2,'Desc 2','Desc 2',1),(3,'Desc 3','Desc 3',2);
/*!40000 ALTER TABLE `informacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_authority`
--

DROP TABLE IF EXISTS `jhi_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_authority` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_authority`
--

LOCK TABLES `jhi_authority` WRITE;
/*!40000 ALTER TABLE `jhi_authority` DISABLE KEYS */;
INSERT INTO `jhi_authority` VALUES ('ROLE_ADMIN'),('ROLE_BOX'),('ROLE_RECEPCION'),('ROLE_USER');
/*!40000 ALTER TABLE `jhi_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_persistent_audit_event`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_persistent_audit_event` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `principal` varchar(50) NOT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `event_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`),
  KEY `idx_persistent_audit_event` (`principal`,`event_date`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_event`
--

LOCK TABLES `jhi_persistent_audit_event` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_event` DISABLE KEYS */;
INSERT INTO `jhi_persistent_audit_event` VALUES (1,'admin','2016-11-07 15:05:00','AUTHENTICATION_SUCCESS'),(2,'admin','2016-11-08 22:05:28','AUTHENTICATION_SUCCESS'),(3,'nicoparola89','2016-11-08 22:13:05','AUTHENTICATION_SUCCESS'),(4,'admin','2016-11-08 22:31:46','AUTHENTICATION_SUCCESS'),(5,'nicoparola89','2016-11-08 22:32:54','AUTHENTICATION_FAILURE'),(6,'nicoparola89','2016-11-08 22:32:59','AUTHENTICATION_FAILURE'),(7,'admin','2016-11-08 23:28:46','AUTHENTICATION_SUCCESS'),(8,'admin','2016-11-08 23:53:03','AUTHENTICATION_SUCCESS'),(9,'admin','2016-11-08 23:59:57','AUTHENTICATION_SUCCESS'),(10,'admin','2016-11-09 00:03:01','AUTHENTICATION_SUCCESS'),(11,'admin','2016-11-09 00:06:13','AUTHENTICATION_SUCCESS'),(12,'asdasd','2016-11-09 00:08:34','AUTHENTICATION_SUCCESS'),(13,'admin','2016-11-09 00:10:15','AUTHENTICATION_SUCCESS'),(14,'admin','2016-11-09 00:12:51','AUTHENTICATION_SUCCESS'),(15,'asdasd','2016-11-09 00:21:50','AUTHENTICATION_FAILURE'),(16,'asdasd','2016-11-09 00:21:53','AUTHENTICATION_SUCCESS'),(17,'admin','2016-11-09 01:16:24','AUTHENTICATION_SUCCESS'),(18,'admin','2016-11-09 01:16:48','AUTHENTICATION_SUCCESS'),(19,'admin','2016-11-09 03:58:13','AUTHENTICATION_SUCCESS'),(20,'admin','2016-11-09 12:24:17','AUTHENTICATION_SUCCESS'),(21,'admin','2016-11-10 23:04:46','AUTHENTICATION_SUCCESS'),(22,'user','2016-11-10 23:35:42','AUTHENTICATION_SUCCESS'),(23,'admin','2016-11-10 23:36:49','AUTHENTICATION_SUCCESS'),(24,'admin','2016-11-13 21:54:17','AUTHENTICATION_SUCCESS'),(25,'asdasd','2016-11-13 21:57:15','AUTHENTICATION_FAILURE'),(26,'asdasd','2016-11-13 21:57:18','AUTHENTICATION_FAILURE'),(27,'asdasd','2016-11-13 21:57:20','AUTHENTICATION_FAILURE'),(28,'asdasd','2016-11-13 21:57:22','AUTHENTICATION_SUCCESS'),(29,'admin','2016-11-13 21:59:37','AUTHENTICATION_SUCCESS'),(30,'asdasd','2016-11-13 22:04:08','AUTHENTICATION_FAILURE'),(31,'asdasd','2016-11-13 22:04:09','AUTHENTICATION_SUCCESS'),(32,'asdasd','2016-11-13 23:06:34','AUTHENTICATION_SUCCESS'),(33,'admin','2016-11-13 23:06:39','AUTHENTICATION_SUCCESS'),(34,'asdasd','2016-11-13 23:07:02','AUTHENTICATION_SUCCESS'),(35,'asdasd','2016-11-13 23:07:21','AUTHENTICATION_SUCCESS'),(36,'admin','2016-11-13 23:07:59','AUTHENTICATION_SUCCESS'),(37,'asdasd','2016-11-13 23:08:18','AUTHENTICATION_SUCCESS'),(38,'asdasd','2016-11-13 23:08:52','AUTHENTICATION_SUCCESS'),(39,'admin','2016-11-14 03:09:45','AUTHENTICATION_SUCCESS'),(40,'asdasd','2016-11-14 03:10:01','AUTHENTICATION_FAILURE'),(41,'asdasd','2016-11-14 03:10:02','AUTHENTICATION_FAILURE'),(42,'asdasd','2016-11-14 03:10:03','AUTHENTICATION_SUCCESS'),(43,'asdasd','2016-11-14 03:11:36','AUTHENTICATION_SUCCESS'),(44,'asdasd','2016-11-14 03:12:27','AUTHENTICATION_SUCCESS'),(45,'asdasd','2016-11-14 03:13:38','AUTHENTICATION_SUCCESS'),(46,'asdasd','2016-11-14 03:14:52','AUTHENTICATION_SUCCESS'),(47,'admin','2016-11-14 03:16:16','AUTHENTICATION_SUCCESS'),(48,'asdasd','2016-11-14 03:16:35','AUTHENTICATION_SUCCESS'),(49,'admin','2016-11-14 03:17:14','AUTHENTICATION_SUCCESS'),(50,'admin','2016-11-14 03:54:10','AUTHENTICATION_SUCCESS'),(51,'admin','2016-11-14 03:55:26','AUTHENTICATION_SUCCESS'),(52,'admin','2016-11-14 03:57:18','AUTHENTICATION_SUCCESS'),(53,'admin','2016-11-14 03:58:01','AUTHENTICATION_SUCCESS'),(54,'user','2016-11-14 04:00:58','AUTHENTICATION_SUCCESS'),(55,'user','2016-11-14 04:04:12','AUTHENTICATION_SUCCESS'),(56,'asdasd','2016-11-14 04:04:32','AUTHENTICATION_SUCCESS'),(57,'admin','2016-11-15 12:27:43','AUTHENTICATION_SUCCESS'),(58,'admin','2016-11-20 22:31:16','AUTHENTICATION_SUCCESS'),(59,'admin','2016-11-23 00:24:22','AUTHENTICATION_SUCCESS'),(60,'admin','2016-11-23 13:55:39','AUTHENTICATION_SUCCESS'),(61,'admin','2016-11-24 22:24:17','AUTHENTICATION_SUCCESS'),(62,'admin','2016-11-25 12:45:37','AUTHENTICATION_SUCCESS'),(63,'admin','2016-12-02 14:43:17','AUTHENTICATION_SUCCESS'),(64,'admin','2016-12-05 12:32:57','AUTHENTICATION_SUCCESS'),(65,'admin','2016-12-16 14:12:47','AUTHENTICATION_SUCCESS'),(66,'admin','2016-12-26 20:55:36','AUTHENTICATION_SUCCESS');
/*!40000 ALTER TABLE `jhi_persistent_audit_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_persistent_audit_evt_data`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_evt_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_persistent_audit_evt_data` (
  `event_id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`,`name`),
  KEY `idx_persistent_audit_evt_data` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_evt_data`
--

LOCK TABLES `jhi_persistent_audit_evt_data` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_evt_data` DISABLE KEYS */;
INSERT INTO `jhi_persistent_audit_evt_data` VALUES (1,'remoteAddress','127.0.0.1'),(1,'sessionId','yMLU714VC90rDibVJIQgHObzPJJn9XD85wtwKfM4'),(2,'remoteAddress','192.168.1.101'),(2,'sessionId','Bj7xtqmSo_YPiHlZD6bU5TYXtzyJwRe29547P1YK'),(3,'remoteAddress','127.0.0.1'),(3,'sessionId','LT3Nr8yzE_v0XlOc07bO1jSHje3sBHwMO2tBOKT1'),(4,'remoteAddress','127.0.0.1'),(4,'sessionId','WBoL-G5v4MXClC4WDGKXcKcY7bgVz4Jg8plu8RRl'),(5,'message','Bad credentials'),(5,'remoteAddress','127.0.0.1'),(5,'sessionId','rSZSBD20eLUkLabvjz8mdazpMkBFSSB4Lwm5XGoZ'),(5,'type','org.springframework.security.authentication.BadCredentialsException'),(6,'message','Bad credentials'),(6,'remoteAddress','127.0.0.1'),(6,'sessionId','wEb6If484TR5sIf5_X6xwI7Iw6oUp1m88KBCLxvY'),(6,'type','org.springframework.security.authentication.BadCredentialsException'),(7,'remoteAddress','127.0.0.1'),(7,'sessionId','aHKmGveFWD3fx6wRLuFb4_m1GD9dGiQPPSuKINbP'),(8,'remoteAddress','127.0.0.1'),(8,'sessionId','KVSzEYvpIKe3Jco6h9AI-h07Xq0Rzyfu0KtfgLo3'),(9,'remoteAddress','127.0.0.1'),(9,'sessionId','wqUNfBcfhEfUbiGZA29ARYjAkrIVqbN128XbZFHJ'),(10,'remoteAddress','127.0.0.1'),(10,'sessionId','2PFHJEcp0eYOtQA8elexvaweqGQe-uS1ovl-E0VL'),(11,'remoteAddress','127.0.0.1'),(11,'sessionId','__XYCyxxysdJFDIT_JGPMn_oCq5-HTtahZcKuD90'),(12,'remoteAddress','127.0.0.1'),(12,'sessionId','qUNigb31cD2Zb2KsKz52EerVLJUIrsCPouZOeKQ4'),(13,'remoteAddress','127.0.0.1'),(13,'sessionId','sWHfdHz7_y_VQ36-Z_YMnC81Vh1aBj-YI33aJhXj'),(14,'remoteAddress','127.0.0.1'),(14,'sessionId','Muc81ocI4JN57RFDnx4fDgOmT3wiawzORzYkEo4t'),(15,'message','Bad credentials'),(15,'remoteAddress','127.0.0.1'),(15,'sessionId','2JRN_hHtAMdplp3ESgku21uFwMvDMYxx7aTX5qOa'),(15,'type','org.springframework.security.authentication.BadCredentialsException'),(16,'remoteAddress','127.0.0.1'),(16,'sessionId','2BVsEvqZ9lcS4qTxyo0pul3RVjBSQnucbb7CW52j'),(17,'remoteAddress','127.0.0.1'),(17,'sessionId','0Rqk3yDF8uB2X4TLq4Z6GduZKnRirOXG2CvuEYHj'),(18,'remoteAddress','127.0.0.1'),(18,'sessionId','YJMB2-JKrsC2SiIcoNwjwWEhtV8ywhH1ZJIufaD5'),(19,'remoteAddress','127.0.0.1'),(19,'sessionId','7RHvFwykGpgGEWfpw5xzw5s_CQjv8JckkBc2iZb-'),(20,'remoteAddress','127.0.0.1'),(20,'sessionId','VEo6MSbFBeDJFQ9SIFKy3kYPS8mHtRU8-a1goydu'),(21,'remoteAddress','127.0.0.1'),(21,'sessionId','X_W_plof4R6G_BMnc5HfnE3EMeO1PkMvzNUh78gN'),(22,'remoteAddress','127.0.0.1'),(22,'sessionId','8K5AIMkzchDFmvD54mm8kln4R04CalLMiDoBpcoq'),(23,'remoteAddress','127.0.0.1'),(23,'sessionId','Iu0bYAcWgrSoMS4Cdt9227N91qLyj_Z8vsPYGqAQ'),(24,'remoteAddress','127.0.0.1'),(24,'sessionId','SFwswew8tmKGyPx4O9mLHx37I0Dz1fIs4caJysML'),(25,'message','Bad credentials'),(25,'remoteAddress','127.0.0.1'),(25,'sessionId','DY5LgGcDkjyCA51MBQWhZgOmkAoacrEUJwHyQrPv'),(25,'type','org.springframework.security.authentication.BadCredentialsException'),(26,'message','Bad credentials'),(26,'remoteAddress','127.0.0.1'),(26,'sessionId','DAKMaLo9cWe3AC0ztzDtpCGS5NxGRuQHBZrOmuqN'),(26,'type','org.springframework.security.authentication.BadCredentialsException'),(27,'message','Bad credentials'),(27,'remoteAddress','127.0.0.1'),(27,'sessionId','WeRiIeL4TE-PcMYil-Hw69rogeShPpfPiLzttmLR'),(27,'type','org.springframework.security.authentication.BadCredentialsException'),(28,'remoteAddress','127.0.0.1'),(28,'sessionId','XPc1COfxSLHqcJPtfhGzjxxtaqrqiloYfNsOT7RI'),(29,'remoteAddress','127.0.0.1'),(29,'sessionId','zrPXrPGl9jnLiuXtBZtVEYW4hFptT66XtDW_GAV9'),(30,'message','Bad credentials'),(30,'remoteAddress','127.0.0.1'),(30,'sessionId','5ohdG7oaNPu4Pd06lAZfwMnaq4PvvMEhyJxp8G4T'),(30,'type','org.springframework.security.authentication.BadCredentialsException'),(31,'remoteAddress','127.0.0.1'),(31,'sessionId','H4vVg36FY5zo4958jz1Ffguh1VI3a8wDN4dAwqfX'),(32,'remoteAddress','127.0.0.1'),(32,'sessionId','s7jq3PFid4Cn4apw0BxferrHufw0xnz4kb7z0zsa'),(33,'remoteAddress','127.0.0.1'),(33,'sessionId','rXHSxQ9lZpAKvhJSKwVs6tnaQ7coxieRuj3-baJ5'),(34,'remoteAddress','127.0.0.1'),(34,'sessionId','WBEHUtYRCRdNGL1wOGtefbIZeR8Zp3lcybVnfaoX'),(35,'remoteAddress','127.0.0.1'),(35,'sessionId','s7WE_KVMCYULzJT8fxm_jmDv7bmnNxB4UHY_wZIZ'),(36,'remoteAddress','127.0.0.1'),(36,'sessionId','XcVO1bS1TKwg1P63323oLP0TQu9lHCCc16wyhr19'),(37,'remoteAddress','127.0.0.1'),(37,'sessionId','MI93MbCaLsV2J5PhPbAcRg5o2qEi_VcxeCAlWfbf'),(38,'remoteAddress','127.0.0.1'),(38,'sessionId','mJNk0sCLHnbAAgHs4ltMDN7nv5sGQmQ_zal6uP8e'),(39,'remoteAddress','127.0.0.1'),(39,'sessionId','qr6LXE8ql7VocGlJtoaoB2d68B4ez2f3tCCt2Dww'),(40,'message','Bad credentials'),(40,'remoteAddress','127.0.0.1'),(40,'sessionId','zXQdox8A6KOV3Sdg-yevsu-kxQ5mKhfNQI4BBNCV'),(40,'type','org.springframework.security.authentication.BadCredentialsException'),(41,'message','Bad credentials'),(41,'remoteAddress','127.0.0.1'),(41,'sessionId','Ic2F3ZClFBZY06UZxCWbUCiM0GVTpczuQnS1WZxO'),(41,'type','org.springframework.security.authentication.BadCredentialsException'),(42,'remoteAddress','127.0.0.1'),(42,'sessionId','sjnmoqnEcsxFnvJEPANHZbjjC3xSxMTtABJIQAPM'),(43,'remoteAddress','127.0.0.1'),(43,'sessionId','JNW5pQHACyidfw9f8w2qyWZkM9SisaRsD3Lc-rKH'),(44,'remoteAddress','127.0.0.1'),(44,'sessionId','3ruKgWhKqqIiR5FiFn7xFBAMUtLbAo88sf3SENdn'),(45,'remoteAddress','127.0.0.1'),(45,'sessionId','UVvQUMAyvu7-muykNA3O2gbRktMcdAZA2ONWCR8n'),(46,'remoteAddress','127.0.0.1'),(46,'sessionId','sqZwYNxvTz7FlobczK-uRtJBiNa4YKAsnnyDkuOh'),(47,'remoteAddress','127.0.0.1'),(47,'sessionId','lbO1JVhdPq9ADiZg-8fIULFP5I6OHerb9xzehNRz'),(48,'remoteAddress','127.0.0.1'),(48,'sessionId','jYqFrsggxJRBxlIM6woTDWTUp2HEdj89PqyVTH19'),(49,'remoteAddress','127.0.0.1'),(49,'sessionId','sa7iRNJiQNnt94He1s9aA-DF8FqdL5djT0UW44y8'),(50,'remoteAddress','127.0.0.1'),(50,'sessionId','gVRGg4bwkJ2_bDoC-b5gcxmpuKoViilbVdZi8ZZ3'),(51,'remoteAddress','127.0.0.1'),(51,'sessionId','KYvZDdKArSefQ83R5FKpiZ6bEMznKz-7Ym9GprpD'),(52,'remoteAddress','127.0.0.1'),(52,'sessionId','EJn3qVBAYO_JXQzl5-fQZ0C2I4n-Xl7oBh6gfY5_'),(53,'remoteAddress','127.0.0.1'),(53,'sessionId','oEWhzXqN9XYE86XAZA-8v0F5CYIii8TlTjjc8wQL'),(54,'remoteAddress','127.0.0.1'),(54,'sessionId','27Bis0xj7IkP55DzFdnvX7WyxdFZi3LISFvXV0UH'),(55,'remoteAddress','127.0.0.1'),(55,'sessionId','VkG7eLDeKj7RvVnkxbv8dpa4TiungA56yFHhXsc-'),(56,'remoteAddress','127.0.0.1'),(56,'sessionId','gOtKL5B9CYNo_EUSbrQzzvQl58wDa1Fy-0FF4vov'),(57,'remoteAddress','127.0.0.1'),(57,'sessionId','-7yEyE8AtcnKw0k5s7RPoOMEamaPQabv7W7_helM'),(58,'remoteAddress','127.0.0.1'),(58,'sessionId','7929-mdD8mLmDa1PJEv0THqHN3lGDWacd843nNWx'),(59,'remoteAddress','127.0.0.1'),(59,'sessionId','8BfvFUTh9WHbIuzXbiJ9QuzQWzJO417pYK3iRXTr'),(60,'remoteAddress','127.0.0.1'),(60,'sessionId','hXP_fMVh6RLEcEtFOXCaVVn5MKWHC0XLcqDy7gv7'),(61,'remoteAddress','127.0.0.1'),(61,'sessionId','Jf10h2qCSJig3r0inBAttnoNnQxD_kmmhySUMdzw'),(62,'remoteAddress','127.0.0.1'),(62,'sessionId','aexFewCJ-WlwhEtxZTPvQ0LqBZaLB4HNsahGFnOB'),(63,'remoteAddress','127.0.0.1'),(63,'sessionId','7MJKyVkj_2k56UcnJxqqBJhRZGYwXuZxegnKWr3I'),(64,'remoteAddress','127.0.0.1'),(64,'sessionId','WoupEQO8bADHgzDC2okcqotomYQgm8JbEDR7re8D'),(65,'remoteAddress','127.0.0.1'),(65,'sessionId','4LxfM9WYDMYU2P4dn-RbBv-gOisbnH74z_hh7Gy4'),(66,'remoteAddress','127.0.0.1'),(66,'sessionId','vj7OP8ZM3XmBVPLheFP_KBw4bDj3g3jtorpUjLKM');
/*!40000 ALTER TABLE `jhi_persistent_audit_evt_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_persistent_token`
--

DROP TABLE IF EXISTS `jhi_persistent_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_persistent_token` (
  `series` varchar(76) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `token_value` varchar(76) NOT NULL,
  `token_date` date DEFAULT NULL,
  `ip_address` varchar(39) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`series`),
  KEY `fk_user_persistent_token` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_token`
--

LOCK TABLES `jhi_persistent_token` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_token` DISABLE KEYS */;
INSERT INTO `jhi_persistent_token` VALUES ('42lEAR38A7UxKWu2CTg8AA==',3,'XTGIsOJnWh3tTB7vYgutlQ==','2016-11-11','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('5MLfOVe7puMpDK3FLByIgg==',3,'N+Va2j5OTdg/CX6PKtpVFQ==','2016-11-14','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('6F0RQ3hjrXgpDIsnxeQfrg==',3,'ub1NVeuROPBvz/vHwkginQ==','2016-11-20','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('7Vi41ybOnodV3K6g6vecnQ==',3,'zLvBrp+gyXFvafReKd52iA==','2016-11-10','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('9squWPcbi5k5zFyJPEhudw==',3,'4KUHYiHF3ch/lqOIfy/QVQ==','2016-11-22','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('As/rfmVG1OKJNkV/EMCeHg==',3,'p9h9YWIXNFXjPnFIFEU/cw==','2016-11-25','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('biJ2HkyZfP9uiMNWsSlztQ==',3,'3z1J1HqMdzbti8/zq2p39g==','2016-11-15','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('gu36KKuL73EIl7dee/12Cg==',3,'M9qCr89kusR2gh+qZEMd4A==','2016-11-08','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('IVrthgtZThCvCxDaeTtSFQ==',3,'VmFZKsNhBOsLnYFi0Xd5lw==','2016-11-24','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0'),('izGL0msnEYXZGMiSE3xqWQ==',3,'EKj/9H8nU+nxpUZ+uisIug==','2016-11-23','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('JE+PAvwu/ffy4zVRydcROg==',8,'jJDdOZMOwBHOvYeJKpt2gg==','2016-11-13','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('JxMsq+AAE3yCh6WTpg0h3w==',3,'wyX31P7TqBNnilkRM+kq5g==','2016-12-05','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('mEmIF5l59VGluuh58QIfxQ==',3,'8g21wjXrIBnfvMlhZA/ntg==','2016-11-09','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('NAXw1yT/v9ZNrEJGq+l0jw==',3,'yAgCMrhmcbV6gWoOtHaRzw==','2016-12-16','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('Nws75sLWqOaJPR1kF4RaIQ==',8,'VfxMauMgRAC9hWXRU5Yp1w==','2016-11-13','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('UTOCZIMB4aDHKO6129TmVw==',8,'V6p4+u+J50/dOkYcIKZtDQ==','2016-11-14','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('vmjFmtJ8uZhYKzsHTVioSA==',3,'p2q7I4Zv1P75e6tKEMIapg==','2016-11-24','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('xuKGm01bkRRvZQrdpusF1Q==',3,'W8MyktObDRmr0epqPAxrBg==','2016-12-02','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),('zLou6HMd7F027zL2s5G8Wg==',3,'mCvhNEb2o97J732A7IcKDg==','2016-12-26','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36');
/*!40000 ALTER TABLE `jhi_persistent_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_user`
--

DROP TABLE IF EXISTS `jhi_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password_hash` varchar(60) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `activated` bit(1) NOT NULL,
  `lang_key` varchar(5) DEFAULT NULL,
  `activation_key` varchar(20) DEFAULT NULL,
  `reset_key` varchar(20) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL,
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `idx_user_login` (`login`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `idx_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user`
--

LOCK TABLES `jhi_user` WRITE;
/*!40000 ALTER TABLE `jhi_user` DISABLE KEYS */;
INSERT INTO `jhi_user` VALUES (1,'system','$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG','System','System','system@localhost','','es',NULL,NULL,'system','2016-11-07 13:10:32',NULL,'system','2016-11-07 13:10:32'),(2,'anonymoususer','$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO','Anonymous','User','anonymous@localhost','','es',NULL,NULL,'system','2016-11-07 13:10:32',NULL,'system','2016-11-07 13:10:32'),(3,'admin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','Administrator','Administrator','admin@localhost','','es',NULL,NULL,'system','2016-11-07 13:10:32',NULL,'system','2016-11-07 13:10:32'),(4,'user','$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K','User','User','user@localhost','','es',NULL,NULL,'system','2016-11-07 13:10:32',NULL,'system','2016-11-07 13:10:32'),(5,'nicoparola89','$2a$10$syL/3LyoQcRYA2EWCZux1eX0KKxhpNwcpzVplDoLZsE/Hm615r4E6','NIcolas','Parola','nicoparola89@gmail.com','','es',NULL,NULL,'admin','2016-11-08 22:02:15',NULL,'admin','2016-11-08 22:11:58'),(8,'asdasd','$2a$10$goFDAEs.g0eIe8NZiPrK8.KvVdszzYu8DTG8wOtKAj4qu0SLw7em2','asdasd','adsasd','leandrotorres73@gmail.com','','es',NULL,NULL,'admin','2016-11-09 00:12:34',NULL,'anonymousUser','2016-11-09 00:12:47');
/*!40000 ALTER TABLE `jhi_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_user_authority`
--

DROP TABLE IF EXISTS `jhi_user_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_name` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`,`authority_name`),
  KEY `fk_authority_name` (`authority_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user_authority`
--

LOCK TABLES `jhi_user_authority` WRITE;
/*!40000 ALTER TABLE `jhi_user_authority` DISABLE KEYS */;
INSERT INTO `jhi_user_authority` VALUES (1,'ROLE_ADMIN'),(3,'ROLE_ADMIN'),(3,'ROLE_BOX'),(3,'ROLE_RECEPCION'),(1,'ROLE_USER'),(3,'ROLE_USER'),(4,'ROLE_USER'),(5,'ROLE_USER');
/*!40000 ALTER TABLE `jhi_user_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `domicilio` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nro_documento` varchar(255) NOT NULL,
  `tipo_documento` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,'asd','asd','123','asd','asd@asd','1321321','DNI'),(8,'Jorgeni','asdadas','123','asdasd','asd@asd','13423412','DNI'),(9,'asd','asd','231','asd','asd@asd','53453454','DNI'),(15,'lea','lae','123','lea','asd@asd','35185498','DNI'),(17,'1','1','1','1','nicoparola89@gmail.com','1231231','DNI'),(19,'asd','asd','1234','asd','asd@asd','34242423','DNI'),(20,'asdasd','asdasd','123','asdasd','asd@asd','2342342','DNI'),(21,'asdasd','asdasd','123','asdasd','asd@asd','32423423','DNI'),(22,'asd','asd','123','asd','asd@asd','5465464','DNI'),(23,'za<ad','asdasd','asdasd','asdasd','asd@asd','9879797','DNI'),(24,'asdasd','asdasd','123','asdasd','asd@asd','4353454','DNI'),(25,'Federica','Ottone','798234','No importa','federicaottone@hotmail.com','39081509','DNI'),(26,'asdasd','asdasd','12312','asdasd','leandrotorres73@gmail.com','5345345','DNI'),(27,'lea','lea','123','lea','leandrotorres73@gmail.com','4234234','DNI'),(28,'asdasd','adasd','123','leasd','leandrotorres73@gmail.com','5435345','DNI'),(29,'fefu','ottone','2314','asd','federicaottone@hotmail.com','39081508','DNI'),(31,'asdasd','asdasd','123','adsasd','asd@asd','35235498','DNI'),(32,'asdasd','asdasd','324','asdasd','asd@asd','43554656','DNI'),(33,'asdasd','asdasdasd','123','asdasd','asd@asd','89768768','DNI'),(35,'asd','asd','123','asd','asd@asd','9787687','DNI'),(37,'asdad','asads','123','sdasd','asd@asd','54353454','DNI'),(38,'asd','asd','123','asd','asd@asd','6546458','DNI');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plantilla_mail`
--

DROP TABLE IF EXISTS `plantilla_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plantilla_mail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `asunto` varchar(255) NOT NULL,
  `contenido` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plantilla_mail`
--

LOCK TABLES `plantilla_mail` WRITE;
/*!40000 ALTER TABLE `plantilla_mail` DISABLE KEYS */;
INSERT INTO `plantilla_mail` VALUES (1,'<tipoTramite>','<horaTurno> <br><nombrePersona> <br>');
/*!40000 ALTER TABLE `plantilla_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_tramite`
--

DROP TABLE IF EXISTS `tipo_tramite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_tramite` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_tipo_turno` varchar(255) NOT NULL,
  `plantilla_mail_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plantilla_mail_id` (`plantilla_mail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_tramite`
--

LOCK TABLES `tipo_tramite` WRITE;
/*!40000 ALTER TABLE `tipo_tramite` DISABLE KEYS */;
INSERT INTO `tipo_tramite` VALUES (1,'Primera vez licencia',1),(2,'Tramite 2',NULL);
/*!40000 ALTER TABLE `tipo_tramite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turno`
--

DROP TABLE IF EXISTS `turno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turno` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `numero_turno` int(11) NOT NULL,
  `fecha_turno` date NOT NULL,
  `estado_id` bigint(20) DEFAULT NULL,
  `persona_id` bigint(20) DEFAULT NULL,
  `tipo_tramite_id` bigint(20) DEFAULT NULL,
  `hora_turno` time NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_turno_estado_id` (`estado_id`),
  KEY `fk_turno_persona_id` (`persona_id`),
  KEY `fk_turno_tipo_tramite_id` (`tipo_tramite_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turno`
--

LOCK TABLES `turno` WRITE;
/*!40000 ALTER TABLE `turno` DISABLE KEYS */;
INSERT INTO `turno` VALUES (1,1,'2017-01-09',1,1,1,'08:00:00'),(11,8,'2016-11-07',1,8,1,'08:00:00'),(18,15,'2016-11-21',2,15,1,'09:00:00'),(21,17,'2016-11-07',2,17,1,'09:00:00'),(22,20,'2016-11-14',1,20,1,'08:00:00'),(23,21,'2016-11-28',2,21,1,'09:00:00'),(24,22,'2016-11-14',2,22,1,'09:00:00'),(25,23,'2016-11-21',2,23,2,'08:00:00'),(26,24,'2016-11-28',2,24,2,'08:00:00'),(27,25,'2016-12-12',2,25,1,'09:00:00'),(28,26,'2016-12-12',2,26,1,'08:00:00'),(29,27,'2016-12-05',2,27,1,'08:00:00'),(30,28,'2016-12-05',2,28,1,'09:00:00'),(31,29,'2016-11-14',2,29,1,'09:00:00'),(33,31,'2016-11-14',3,31,1,'12:00:00'),(34,32,'2016-11-24',3,32,1,'12:00:00'),(35,33,'2016-11-24',3,33,1,'13:00:00'),(37,35,'2017-01-05',1,35,1,'21:00:00'),(39,37,'2016-12-08',1,37,1,'12:00:00'),(40,38,'2016-12-19',1,38,2,'08:00:00');
/*!40000 ALTER TABLE `turno` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-26 19:28:57
