(function() {
    'use strict';

    var myApp = angular
        .module('myApp')
        .controller('TurnoController', TurnoController);

    myApp.filter('dateFilter', function(){
        return function(arr, field, val){
            var filtered = [];
            angular.forEach(arr, function(item){
                if(moment(item.fechaTurno).format('DD/MM/YYYY').indexOf(field) > -1 || field == undefined)
                    filtered.push(item);
            });
            return filtered;
        }
    });

    TurnoController.$inject = ['$scope', 'Turno'];

    function TurnoController ($scope, Turno) {
        var vm = this;
        
        vm.turnos = [];
        $scope.search = '';
        $scope.fechaTurno = '';
        $scope.userSearch = '';

        loadAll();

        function loadAll() {
            Turno.query(function(result) {
                vm.turnos = result;
            });
        }

        $scope.printData = function()
        {
            $('.no-show').hide();
            $("#turnos td:last-child").hide();
            var divToPrint=document.getElementById("turnos");
            var newWin= window.open("");
            newWin.document.write(divToPrint.outerHTML);
            newWin.print();
            newWin.close();
            $('#search-header').show();
            $("#turnos td:last-child").show();
        }

        $scope.delete = function (id)
        {
            Turno.delete(id,
            function () {
                loadAll();
            });
        }
    }
})();
