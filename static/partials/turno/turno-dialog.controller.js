(function() {
    'use strict';

    angular
        .module('myApp')
        .controller('TurnoDialogController', TurnoDialogController);

    TurnoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Turno', 'EstadoTurno', 'Persona', 'TipoTramite'];

    function TurnoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Turno, EstadoTurno, Persona, TipoTramite) {
        var vm = this;

        vm.turno = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.estadoturnos = EstadoTurno.query();
        vm.personas = Persona.query();
        vm.tipotramites = TipoTramite.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.turno.id !== null) {
                Turno.update(vm.turno, onSaveSuccess, onSaveError);
            } else {
                Turno.save(vm.turno, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('myApp:turnoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.fechaTurno = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
