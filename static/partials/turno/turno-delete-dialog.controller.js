(function() {
    'use strict';

    angular
        .module('myApp')
        .controller('TurnoDeleteController',TurnoDeleteController);

    TurnoDeleteController.$inject = ['$uibModalInstance', 'entity', 'Turno'];

    function TurnoDeleteController($uibModalInstance, entity, Turno) {
        var vm = this;

        vm.turno = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Turno.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
