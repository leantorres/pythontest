(function() {
    'use strict';

    angular
        .module('myApp')
        .controller('TurnoDetailController', TurnoDetailController);

    TurnoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Turno', 'EstadoTurno', 'Persona', 'TipoTramite'];

    function TurnoDetailController($scope, $rootScope, $stateParams, previousState, entity, Turno, EstadoTurno, Persona, TipoTramite) {
        var vm = this;

        vm.turno = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('myApp:turnoUpdate', function(event, result) {
            vm.turno = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
