(function() {
    'use strict';
    angular
        .module('myApp')
        .factory('Turno', Turno);

    Turno.$inject = ['$resource', 'DateUtils'];

    function Turno ($resource, DateUtils) {
        var resourceUrl =  'api/turnos/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.fechaTurno = DateUtils.convertLocalDateFromServer(data.fechaTurno);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.fechaTurno = DateUtils.convertLocalDateToServer(copy.fechaTurno);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.fechaTurno = DateUtils.convertLocalDateToServer(copy.fechaTurno);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
