(function() {
    'use strict';

    angular
        .module('myApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('turno', {
            parent: 'entity',
            url: '/turno',
            views: {
                'content@': {
                    templateUrl: 'partials/turno/turnos.html',
                    controller: 'TurnoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('turno');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('turno-detail', {
            parent: 'entity',
            url: '/turno/{id}',
            views: {
                'content@': {
                    templateUrl: 'partials/turno/turno-detail.html',
                    controller: 'TurnoDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('turno');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Turno', function($stateParams, Turno) {
                    return Turno.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'turno',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('turno-detail.edit', {
            parent: 'turno-detail',
            url: '/detail/edit',
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'partials/turno/turno-dialog.html',
                    controller: 'TurnoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Turno', function(Turno) {
                            return Turno.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('turno.new', {
            parent: 'turno',
            url: '/new',
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'partials/turno/turno-dialog.html',
                    controller: 'TurnoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                numeroTurno: null,
                                fechaTurno: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('turno', null, { reload: 'turno' });
                }, function() {
                    $state.go('turno');
                });
            }]
        })
        .state('turno.edit', {
            parent: 'turno',
            url: '/{id}/edit',
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'partials/turno/turno-dialog.html',
                    controller: 'TurnoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Turno', function(Turno) {
                            return Turno.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('turno', null, { reload: 'turno' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('turno.delete', {
            parent: 'turno',
            url: '/{id}/delete',
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/turno/turno-delete-dialog.html',
                    controller: 'TurnoDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Turno', function(Turno) {
                            return Turno.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('turno', null, { reload: 'turno' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
