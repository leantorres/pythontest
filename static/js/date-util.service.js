(function() {
    'use strict';

    angular
        .module('myApp')
        .factory('DateUtils', DateUtils);

    DateUtils.$inject = ['$filter'];

    function DateUtils ($filter) {

        var service = {
            convertDateTimeFromServer : convertDateTimeFromServer,
            convertLocalDateFromServer : convertLocalDateFromServer,
            convertLocalDateToServer : convertLocalDateToServer,
            convertListTimeFromServer : convertListTimeFromServer,
            convertTimeFromServer : convertTimeFromServer,
            convertTimeToServer : convertTimeToServer,
            showDateNiceFormat : showDateNiceFormat,
            dateformat : dateformat
        };

        return service;

        function convertDateTimeFromServer (date) {
            if (date) {
                return new Date(date);
            } else {
                return null;
            }
        }

        function convertLocalDateFromServer (date) {
            if (date) {
                var dateString = date.split('-');
                return new Date(dateString[0], dateString[1] - 1, dateString[2]);
            }
            return null;
        }

        function convertLocalDateToServer (date) {
            if (date) {
                return $filter('date')(date, 'yyyy-MM-dd');
            } else {
                return null;
            }
        }

        function convertTimeFromServer(time)
        {
            var lastIndex = time.lastIndexOf(":");
            return time.substring(0, lastIndex);
        }

        function convertListTimeFromServer(listTime)
        {
            for (var i in listTime)
            {
                var lastIndex = listTime[i].horario.lastIndexOf(":");
                listTime[i].horario = listTime[i].horario.substring(0, lastIndex);
            }
            return listTime;
        }

        function convertTimeToServer(time){
            return time + ":00";
        }

        function dateformat () {
            return 'yyyy-MM-dd';
        }

        function showDateNiceFormat(date)
        {
            var dd = date.getDate();
            var mm = date.getMonth()+1; //January is 0!
            var yyyy = date.getFullYear();

            if(dd<10) {
                dd='0'+dd
            }

            if(mm<10) {
                mm='0'+mm
            }

            date = dd+'/'+mm+'/'+yyyy;
            return date;
        }
    }

})();
