'use strict';   // See note about 'use strict'; below

var myApp = angular.module('myApp', ['ngRoute','ngResource']);

myApp.config(['$routeProvider',
     function($routeProvider) {
         $routeProvider.
             when('/', {
                 templateUrl: '/static/partials/index.html',
             }).
             when('/turno', {
                templateUrl: '/static/partials/turno/turnos.html',
                controller: 'TurnoController',
                controllerAs: 'vm'
            }).
             otherwise({
                 redirectTo: '/'
             });
    }]);