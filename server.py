from flask import Flask, Response, jsonify, redirect, url_for, render_template
from models import Turno, Persona
from dateutil import DateTimeEncoder

app = Flask(__name__)

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route("/api/turnos")
def turnos():
    results = Turno.query.all()
    json_results = []
    for result in results:
        d = {'id': result.id,
             'fechaTurno': result.fecha_turno.strftime('%Y-%m-%d'),
             'horaTurno': DateTimeEncoder().encode(result.hora_turno),
             'persona_id': result.persona_id,
             'personaNombre': result.persona.nombre,
             'apellido': result.persona.apellido,
             'tipo_tramite_id': result.tipo_tramite.id,
             'tipoTramiteNombre': result.tipo_tramite.nombre_tipo_turno,
             'estado_id': result.estado.id,
             'estadoNombre': result.estado.nombre_estado_turno}
        json_results.append(d)
    return jsonify(
        json_results)

@app.route("/api/turnos/<int:id>", methods =['DELETE'])
def deleteReservation(id):
    Turno.query.filter_by(id=id).delete()
    return "success"

if __name__ == "__main__":
    app.run(debug=True)
