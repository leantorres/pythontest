from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Table, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://test:test@localhost/turnoslicencia'
db = SQLAlchemy(app)

class Turno(db.Model):
  __tablename__ = "turno"
  id = db.Column(db.Integer, primary_key=True)
  fecha_turno = db.Column(db.String, nullable=False)
  estado_id = db.Column(db.Integer, ForeignKey('estado_turno.id'))
  estado = relationship("EstadoTurno", uselist=False)
  persona_id = db.Column(db.Integer, ForeignKey('persona.id'))
  persona = relationship("Persona", uselist=False)
  tipo_tramite_id = db.Column(db.Integer, ForeignKey('tipo_tramite.id'))
  tipo_tramite = relationship("TipoTramite")
  hora_turno = db.Column(db.String, nullable=False)

  def __init__(self, estado_id, fecha_turno, persona_id, tipo_tramite_id, hora_turno):
    self.fecha_turno = fecha_turno
    self.estado_id = estado_id
    self.persona_id = persona_id
    self.tipo_tramite_id = tipo_tramite_id
    self.hora_turno = hora_turno

  def __repr__(self):
    return '<id {}'.format(self.id)

class Persona(db.Model):

  __tablename__ = "persona"

  id = db.Column(db.Integer, primary_key=True)
  nombre = db.Column(db.String, nullable=False)
  apellido = db.Column(db.String, nullable=False)
  telefono = db.Column(db.String, nullable=False)
  email = db.Column(db.String, nullable=False)
  nro_documento = db.Column(db.String, nullable=False)
  tipo_documento = db.Column(db.String, nullable=False)

  def __init__(self, nombre, apellido, telefono, email, nro_documento, tipo_documento):
    self.nombre = nombre
    self.apellido = apellido
    self.telefono = telefono
    self.email = email
    self.nro_documento = nro_documento
    self.tipo_documento = tipo_documento

  def __repr__(self):
    return '<id {}'.format(self.id)

class TipoTramite(db.Model):

  __tablename__ = "tipo_tramite"

  id = db.Column(db.Integer, primary_key=True)
  nombre_tipo_turno = db.Column(db.String, nullable=False)

  def __init__(self, nombre_tipo_turno):
    self.nombre_tipo_turno = nombre_tipo_turno

  def __repr__(self):
    return '<nombre_tipo_turno {}'.format(self.nombre_tipo_turno)


class EstadoTurno(db.Model):

  __tablename__ = "estado_turno"

  id = db.Column(db.Integer, primary_key=True)
  nombre_estado_turno = db.Column(db.String, nullable=False)

  def __init__(self, nombre_estado_turno):
    self.nombre_estado_turno = nombre_estado_turno

  def __repr__(self):
    return '<nombre_estado_turno {}'.format(self.nombre_estado_turno)